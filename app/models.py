from django.db import models


class Record(models.Model):
    status = models.CharField(max_length=255)
    progress = models.FloatField(default=0)
    counter = models.BigIntegerField(default=0)
