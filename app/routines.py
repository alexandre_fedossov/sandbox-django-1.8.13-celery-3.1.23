import random

from celery import Task, chain, chord, group
from celery.utils.log import get_task_logger
from celery_once import QueueOnce
from django.conf import settings
from django.core.cache import caches
from django.db.models import F

from .models import Record

logger = get_task_logger(__name__)


class StatusesEnum(object):
    NEW = 'NEW'
    TASK_A = 'TASK_A'
    TASK_B = 'TASK_B'
    TASK_C = 'TASK_C'
    DONE = 'DONE'


# region native implementation
class BaseTask(Task):
    abstract = True

    status = None
    progress = None

    def run(self, *args, **kwargs):
        logger.debug('%s (%s) task run (%s retry count)', self.request.id, self.__class__.__name__, self.request.retries)

        if self.request.retries < 3:
            self.retry(countdown=random.randint(5, 10), kwargs=kwargs)

        logger.debug('%s (%s) task run payload', self.request.id, self.__class__.__name__)
        Record.objects.filter(pk=kwargs.get('record')).update(
            status=self.status,
            counter=F('counter') + 1,
            progress=self.progress or F('progress') + kwargs.get('progress', 0))


class TaskA(BaseTask):
    status = StatusesEnum.TASK_A


class TaskB(BaseTask):
    status = StatusesEnum.TASK_B


class TaskC(BaseTask):
    status = StatusesEnum.TASK_C


class FinalTask(BaseTask):
    status = StatusesEnum.DONE
    progress = 100


class ResetPercentage(Task):
    def run(self, *args, **kwargs):
        Record.objects.filter(pk=kwargs.get('record')).update(progress=0)


class MainTask(Task):
    def run(self, *args, **kwargs):
        record = Record.objects.create(status=StatusesEnum.NEW)

        count = 100
        kw = dict(record=record.pk, progress=1. / count)

        chain(
            chord(group([TaskA().subtask(kwargs=kw) for _ in range(count)]), ResetPercentage().subtask(kwargs=kw)),
            chord(group([TaskB().subtask(kwargs=kw) for _ in range(count)]), ResetPercentage().subtask(kwargs=kw)),
            chord(group([TaskC().subtask(kwargs=kw) for _ in range(count)]), ResetPercentage().subtask(kwargs=kw)),
            group([FinalTask().subtask(kwargs=kw)])
        ).apply_async()


# endregion

# region QueueOnce implementation
# noinspection PyAbstractClass
class CustomKeyQueueOnce(QueueOnce):
    abstract = True

    def get_key(self, args=None, kwargs=None):
        return 'Lock_%s_%s_%s' % (self.__class__.__name__, self.request.id, self.request.retries)


class QueueOnceBaseTask(CustomKeyQueueOnce):
    abstract = True

    status = None
    progress = None

    def run(self, *args, **kwargs):
        logger.debug('%s (%s) task run (%s retry count)', self.request.id, self.__class__.__name__, self.request.retries)

        if self.request.retries < 3:
            self.retry(countdown=random.randint(5, 10), kwargs=kwargs)

        logger.debug('%s (%s) task run payload', self.request.id, self.__class__.__name__)
        Record.objects.filter(pk=kwargs.get('record')).update(
            status=self.status,
            counter=F('counter') + 1,
            progress=self.progress or F('progress') + kwargs.get('progress', 0))


class QueueOnceTaskA(QueueOnceBaseTask):
    status = StatusesEnum.TASK_A


class QueueOnceTaskB(QueueOnceBaseTask):
    status = StatusesEnum.TASK_B


class QueueOnceTaskC(QueueOnceBaseTask):
    status = StatusesEnum.TASK_C


class QueueOnceFinalTask(QueueOnceBaseTask):
    status = StatusesEnum.DONE
    progress = 100


class QueueOnceResetPercentage(CustomKeyQueueOnce):
    def run(self, *args, **kwargs):
        Record.objects.filter(pk=kwargs.get('record')).update(progress=0)


class QueueOnceMainTask(CustomKeyQueueOnce):
    once = {'graceful': True}

    def run(self, *args, **kwargs):
        record = Record.objects.create(status=StatusesEnum.NEW)

        count = 100
        kw = dict(record=record.pk, progress=1. / count)

        chain(
            chord(group([QueueOnceTaskA().subtask(kwargs=kw) for _ in range(count)]), QueueOnceResetPercentage().subtask(kwargs=kw)),
            chord(group([QueueOnceTaskB().subtask(kwargs=kw) for _ in range(count)]), QueueOnceResetPercentage().subtask(kwargs=kw)),
            chord(group([QueueOnceTaskC().subtask(kwargs=kw) for _ in range(count)]), QueueOnceResetPercentage().subtask(kwargs=kw)),
            group([QueueOnceFinalTask().subtask(kwargs=kw)])
        ).apply_async()


# endregion

# region Custom implementation
# noinspection PyAbstractClass
class TaskWithLock(Task):
    """
    Base task with lock to prevent multiple execution of tasks with ETA.
    It's happens with multiple workers for tasks with any delay (countdown, ETA).
    You may override cache backend by setting `CELERY_TASK_LOCK_CACHE` in your Django settings file
    """
    abstract = True
    cache = caches[getattr(settings, 'CELERY_TASK_LOCK_CACHE', 'default')]
    lock_expiration = 60 * 60 * 24  # 1 day

    @property
    def lock_key(self):
        """
        Unique string for task as lock key
        """
        return 'TaskLock_%s_%s_%s' % (self.__class__.__name__, self.request.id, self.request.retries)

    def acquire_lock(self):
        """
        Set lock
        """
        result = self.cache.add(self.lock_key, True, self.lock_expiration)
        logger.debug('Acquiring %s key %s', self.lock_key, 'succeed' if result else 'failed')
        return result

    def __call__(self, *args, **kwargs):
        """
        Checking for lock existence
        """
        if self.acquire_lock():
            logger.debug('Task %s execution with lock started', self.request.id)
            return super(TaskWithLock, self).__call__(*args, **kwargs)
        logger.warning('Task %s skipped due lock detection', self.request.id)


class CustomBaseTask(TaskWithLock):
    abstract = True

    status = None
    progress = None

    def run(self, *args, **kwargs):
        logger.debug('%s (%s) task run (%s retry count)', self.request.id, self.__class__.__name__, self.request.retries)

        if self.request.retries < 3:
            self.retry(countdown=random.randint(5, 10), kwargs=kwargs)

        logger.debug('%s (%s) task run payload', self.request.id, self.__class__.__name__)
        Record.objects.filter(pk=kwargs.get('record')).update(
            status=self.status,
            counter=F('counter') + 1,
            progress=self.progress or F('progress') + kwargs.get('progress', 0))


class CustomBaseTaskTaskA(CustomBaseTask):
    status = StatusesEnum.TASK_A


class CustomBaseTaskTaskB(CustomBaseTask):
    status = StatusesEnum.TASK_B


class CustomBaseTaskTaskC(CustomBaseTask):
    status = StatusesEnum.TASK_C


class CustomBaseTaskFinalTask(CustomBaseTask):
    status = StatusesEnum.DONE
    progress = 100


class CustomBaseTaskResetPercentage(TaskWithLock):
    def run(self, *args, **kwargs):
        Record.objects.filter(pk=kwargs.get('record')).update(progress=0)


class CustomBaseTaskMainTask(TaskWithLock):
    def run(self, *args, **kwargs):
        record = Record.objects.create(status=StatusesEnum.NEW)

        count = 100
        kw = dict(record=record.pk, progress=1. / count)

        chain(
            chord(group([CustomBaseTaskTaskA().subtask(kwargs=kw) for _ in range(count)]), CustomBaseTaskResetPercentage().subtask(kwargs=kw)),
            chord(group([CustomBaseTaskTaskB().subtask(kwargs=kw) for _ in range(count)]), CustomBaseTaskResetPercentage().subtask(kwargs=kw)),
            chord(group([CustomBaseTaskTaskC().subtask(kwargs=kw) for _ in range(count)]), CustomBaseTaskResetPercentage().subtask(kwargs=kw)),
            group([CustomBaseTaskFinalTask().subtask(kwargs=kw)])
        ).apply_async()

# endregion
