from celery import chain, chord, group
from django.test import TestCase

from .models import Record
from .routines import (
    CustomBaseTaskTaskA, CustomBaseTaskTaskB, CustomBaseTaskTaskC,
    CustomBaseTaskFinalTask, CustomBaseTaskResetPercentage, StatusesEnum)


class CustomTaskTestCase(TestCase):
    def test_no_duplicates(self):
        record = Record.objects.create(status=StatusesEnum.NEW)

        count = 100
        kw = dict(record=record.pk, progress=1. / count)

        chain(
            chord(group([CustomBaseTaskTaskA().subtask(kwargs=kw) for _ in range(count)]), CustomBaseTaskResetPercentage().subtask(kwargs=kw)),
            chord(group([CustomBaseTaskTaskB().subtask(kwargs=kw) for _ in range(count)]), CustomBaseTaskResetPercentage().subtask(kwargs=kw)),
            chord(group([CustomBaseTaskTaskC().subtask(kwargs=kw) for _ in range(count)]), CustomBaseTaskResetPercentage().subtask(kwargs=kw)),
            group([CustomBaseTaskFinalTask().subtask(kwargs=kw)])
        )().get()

        record.refresh_from_db(fields=('status', 'counter'))

        self.assertEquals(record.status, StatusesEnum.DONE, 'Wrong final status of record. Check task execution order in logs')
        self.assertEquals(record.counter, 301, 'Wrong counter values of task execution times')
