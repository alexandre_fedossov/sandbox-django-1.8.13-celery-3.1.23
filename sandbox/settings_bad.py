# noinspection PyUnresolvedReferences
from .settings import *

BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 1}
