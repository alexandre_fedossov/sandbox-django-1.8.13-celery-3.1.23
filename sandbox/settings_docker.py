from .settings import *

DEBUG = False
ALLOWED_HOSTS = ['*']

CACHES['default']['LOCATION'] = 'redis://redis:6379/1'

# Celery settings
BROKER_URL = 'redis://redis:6379/2'

# CeleryOnce settings
ONCE_REDIS_URL = 'redis://redis:6379/0'
