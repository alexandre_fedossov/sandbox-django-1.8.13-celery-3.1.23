# noinspection PyUnresolvedReferences
from .settings_docker import *

BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 1}
