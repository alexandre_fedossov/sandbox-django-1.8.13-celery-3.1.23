# Sandbox for Celery

## Issue #1
Multiple task execution when ETA/countdown/retry tasks used. More information [here](http://celery.readthedocs.io/en/latest/getting-started/brokers/redis.html#id1). In this test case we using 3 different tasks in groups and execute 100 times each. Each of that task retrying multiple times with random `countdown` value. After that we using single one task to finalize result. All results placed in `app_record` table.

### Steps to reproduce
Update settings with transport options:
```python
BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 1}
```
Schedule async task:
```python
from app.routines import MainTask
MainTask().apply_async()
```

### Expected results

There should be variable count of execution times. For example:

|ID|status|progress|counter|
|::|:----:|:------:|:-----:|
|12| DONE |   100  |  668  |
|13| DONE |   100  |  662  |
|14| DONE |   100  |  614  |

As you can see there are counters with more that 301 task execution. Also you may notice in logs something like that (multiple workers executed tasks with same TaskID):
```
TaskC[fe89cdd7-5998-4de4-a404-45373bd0f8fc] succeeded in 0.0147672670064s: None
TaskC[fe89cdd7-5998-4de4-a404-45373bd0f8fc] succeeded in 0.0421458979981s: None

TaskC[ff19d285-1193-4e03-813f-858a08c5779d] succeeded in 0.0277261390002s: None
TaskC[ff19d285-1193-4e03-813f-858a08c5779d] succeeded in 0.0375140100005s: None
```

### Solution
Update settings with transport `visibility_timeout` option with value more than biggest ETA/countdown for example:
```python
BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 60*60*24} # 1 day
```

Now you should expecting this:

|ID|status|progress|counter|
|::|:----:|:------:|:-----:|
|15| DONE |   100  |  301  |
|16| DONE |   100  |  301  |
|17| DONE |   100  |  301  |

Use following snippet for cases when you need to schedule task over `visibility_timeout` value in future:
```python
from celery import Task
from django.conf import settings
from django.core.cache import caches
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

# noinspection PyAbstractClass
class TaskWithLock(Task):
    """
    Base task with lock to prevent multiple execution of tasks with ETA.
    It's happens with multiple workers for tasks with any delay (countdown, ETA).
    You may override cache backend by setting `CELERY_TASK_LOCK_CACHE` in your Django settings file
    """
    abstract = True
    cache = caches[getattr(settings, 'CELERY_TASK_LOCK_CACHE', 'default')]
    lock_expiration = 60 * 60 * 24  # 1 day

    @property
    def lock_key(self):
        """
        Unique string for task as lock key
        """
        return 'TaskLock_%s_%s_%s' % (self.__class__.__name__, self.request.id, self.request.retries)

    def acquire_lock(self):
        """
        Set lock
        """
        result = self.cache.add(self.lock_key, True, self.lock_expiration)
        logger.debug('Acquiring %s key %s', self.lock_key, 'succeed' if result else 'failed')
        return result

    def __call__(self, *args, **kwargs):
        """
        Checking for lock existence
        """
        if self.acquire_lock():
            logger.debug('Task %s execution with lock started', self.request.id)
            return super(TaskWithLock, self).__call__(*args, **kwargs)
        logger.warning('Task %s skipped due lock detection', self.request.id)
```