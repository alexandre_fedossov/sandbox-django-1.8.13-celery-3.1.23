FROM python:2.7
MAINTAINER Alexandre Fedossov <alexandre.fedossov@leverx.com>

RUN mkdir /app
WORKDIR app

VOLUME app
EXPOSE 8000

ENV DJANGO_SETTINGS_MODULE=sandbox.settings_docker

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

CMD ["python", "manage.py", "runserver", "--insecure", "0.0.0.0:8000"]
